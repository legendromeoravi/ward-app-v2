import { View, Text } from 'react-native'
import React from 'react'
import { Tabs } from 'expo-router'
import { Feather, Ionicons, MaterialIcons } from '@expo/vector-icons'

const TabsLayout = () => {
  return (
    <Tabs screenOptions={{
      tabBarActiveTintColor: 'green',
      tabBarLabelStyle: {
        fontSize: 13,
        paddingBottom: 8
      },
      tabBarStyle: {
        height: 65,
        paddingVertical: 2
      },
    }}>

      <Tabs.Screen
        options={{
          tabBarIcon: ({ color }) => <Feather name='home' size={25} color={color} />,
          tabBarLabel: 'Home',
          headerShown: false
        }}
        name='home'
      />

      <Tabs.Screen
        options={{
          tabBarIcon: ({ color }) => <MaterialIcons name="message" size={25} color={color} />,
          tabBarLabel: 'Notice'
        }}
        name='notice' />


      <Tabs.Screen
        options={{
          tabBarIcon: ({ color }) => <MaterialIcons name='how-to-vote' size={25} color={color} />,
          tabBarLabel: 'FeedBack'
        }}
        name='feedback' />


      <Tabs.Screen
        options={{
          tabBarIcon: ({ color }) => <Ionicons name='call' size={25} color={color} />,
          tabBarLabel: 'Phone'
        }}
        name='phone' />
    </Tabs>
  )
}

export default TabsLayout