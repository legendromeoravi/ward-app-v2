import { View, Text, ScrollView } from 'react-native'
import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context'
import HomeHeader from '../../components/home/homeHeader'
import HomeBody from '../../components/home/homeBody'

const Home = () => {
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: 'green'}}>

      <HomeHeader />

      <HomeBody />

    </SafeAreaView>
  )
}

export default Home