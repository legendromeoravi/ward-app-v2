import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

const HomeTitle = ({ title }) => {

    return (
        <Text style={styles.title}> {title} </Text>
    )
}

export default HomeTitle

const styles = StyleSheet.create({

    title: {
        color: 'green',
        fontSize: 18,
        fontWeight: '700',
        borderBottomColor: 'red',
        borderBottomWidth: 2,
        alignSelf: 'flex-start',
        marginVertical: 18
    }

})