import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { widthPercentageToDP } from 'react-native-responsive-screen'
import { Feather } from '@expo/vector-icons'

const HomeFeautre = ({title, icon}) => {
  return (
    <View style={styles.container}>
      <View style={styles.iconContainer}>
        {icon}
      </View>
      <Text style={styles.title}>{title} </Text>
    </View>
  )
} 

export default HomeFeautre

const styles = StyleSheet.create({

    container: {
        backgroundColor: 'green',
        height: 70,
        width: widthPercentageToDP(43),
        borderRadius: 12,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 10,
        gap: 10
    },

    iconContainer: {
        width: '40%',
        height: '100%',
        backgroundColor: 'white',
        borderRadius: 9,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    },

    title: {
        fontWeight: '700',
        color: 'white',
        width: '50%',
        textAlign: 'start'
    }

})