import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen'

const HomeHeader = () => {
    return (
        <View style={styles.main}>


            {/* Main header */}
            <View style={styles.mainHeader}>
                <Image source={require('../../assets/images/logo-v.png')} style={styles.mainLogo} />
                <Text style={styles.headerTitle}>बारबर्दिया नगरपालिका वडा .नं ७, बर्दिया</Text>
                <Image source={require('../../assets/images/nepal_flag.gif')} style={styles.flag} />
            </View>


            {/* Greeting */}
            <View style={styles.greeting}>
                <Image source={require('../../assets/images/namaste.png')} style={styles.namaste} />
                <Image source={require('../../assets/images/ward_chairmen.jpg')} style={styles.namaste} />
            </View>

            {/* Slogan */}
            <View style={styles.sloganContainer}>
                <Text style={styles.sloganText}> "Clean वडा Green वडा" </Text>
            </View>

        </View>
    )
}

export default HomeHeader

const styles = StyleSheet.create({

    main: {
        width: widthPercentageToDP(100),
        // height: heightPercentageToDP(33),
        paddingHorizontal: widthPercentageToDP(5),
        paddingVertical: widthPercentageToDP(2),
        flex: 1
    },

    // Main Header
    mainHeader: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 20
    },

    mainLogo: {
        width: widthPercentageToDP(12),
        height: widthPercentageToDP(12)
    },

    flag: {
        width: widthPercentageToDP(14),
        height: widthPercentageToDP(14),
        objectFit: 'contain'
    },

    headerTitle: {
        color: 'white',
        fontSize: 17,
        fontWeight: '800',
        width: widthPercentageToDP(50),
        textAlign: 'center'
    },


    // Greeting
    greeting: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    namaste: {
        width: widthPercentageToDP(30),
        objectFit: 'contain',
        height: heightPercentageToDP(15),
    },

    // Slogan
    sloganContainer: {
        backgroundColor: 'white',
        paddingVertical: 8,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 100
    },

    sloganText: {
        color: 'green',
        fontWeight: '700'
    }
})