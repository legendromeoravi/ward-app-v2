import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React, { useState } from 'react'
import { heightPercentageToDP, widthPercentageToDP } from 'react-native-responsive-screen'
import HomeTitle from '../homeTitle'
import HomeFeautre from './homeFeature'
import { Feather, FontAwesome, FontAwesome5, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons'
import Carousel from 'react-native-reanimated-carousel'

const HomeBody = () => {

    const featureIconSize = 25
    const featureIconColor = 'green'

    const [paginationIndex, setPaginationIndex] = useState(0)

    const items = [
        (
            <View style={styles.featureContainer}>
                <HomeFeautre icon={<FontAwesome name='user' size={featureIconSize} color={featureIconColor} />} title={'परिचय'} />
                <HomeFeautre icon={<MaterialCommunityIcons name='shield-account-variant' size={featureIconSize} color={featureIconColor} />} title={'शुसासन'} />
                <HomeFeautre icon={<MaterialIcons name='message' size={featureIconSize} color={featureIconColor} />} title={'सुचना तथा जानकारी'} />
                <HomeFeautre icon={<FontAwesome name='building' size={featureIconSize} color={featureIconColor} />} title={'संघ संस्था'} />
            </View>
        ),

        (
            <View style={styles.featureContainer}>
                <HomeFeautre icon={<MaterialIcons name='photo-library' size={featureIconSize} color={featureIconColor} />} title={'ग्यालेरी'} />
                <HomeFeautre icon={<FontAwesome5 name='file-contract' size={featureIconSize} color={featureIconColor} />} title={'पंजीकरण'} />
                <HomeFeautre icon={<MaterialIcons name='how-to-vote' size={featureIconSize} color={featureIconColor} />} title={'गुनासो तथा सुझाव'} />
                <HomeFeautre icon={<FontAwesome name='phone' size={featureIconSize} color={featureIconColor} />} title={'महत्त्वपूर्ण .नं'} />
            </View>
        ),

        (
            <View style={[styles.featureContainer, { justifyContent: 'flex-start' }]}>
                <HomeFeautre icon={<MaterialCommunityIcons name='information' size={featureIconSize} color={featureIconColor} />} title={'निर्माता जानकारी'} />
            </View>
        )
    ]


    return (
        <View style={styles.main}>

            <HomeTitle title={'फिचर्स'} />


            <View style={{ height: 150 }}>
                <Carousel
                    width={widthPercentageToDP(90)}
                    data={items}
                    loop={false}
                    onSnapToItem={(index) => {setPaginationIndex(index)}}
                    renderItem={({ item, index }) => item}
                />
            </View>

            <View style={styles.paginationContainer}>
                {
                    items.map((item, index) => <View style={[styles.pagination, { backgroundColor: ( index == paginationIndex ? 'green' : 'lightgray') } ]}></View>)
                }
            </View>





        </View>
    )
}

export default HomeBody

const styles = StyleSheet.create({

    main: {
        flex: 2,
        display: 'flex',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        backgroundColor: 'white',

        width: '100%',
        borderTopRightRadius: 40,
        borderTopLeftRadius: 40,
        marginTop: 20,
        paddingTop: 20,
        paddingHorizontal: widthPercentageToDP(5)
    },

    featureContainer: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'row',
        gap: 10,
        width: widthPercentageToDP(90),
        flexWrap: 'wrap'
    },

    paginationContainer: {
        width: widthPercentageToDP(90),
        height: 40,
        marginTop: 5,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        gap: 4,
    },

    pagination: {
        width: 10,
        height: 10,
        borderRadius: 200,
    }

})